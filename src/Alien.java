import java.util.List;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.Graphics2D;


public class Alien{
    
    private double posX;
    private double posY;
    private boolean estMort;
    private int nbTour;
    private EnsembleChaines forme;

    /**Constructeur de la classe Alien
     * 
     * @param posX position sur l'axe des abscisse d'un alien
     * @param posY position sur l'axe des ordonnées d'un alien
     */
    public Alien(double posX, double posY){
        this.posX = posX;
        this.posY = posY;
        this.estMort = false;
    }

    /**Permet de retourner la position X d'un alien
     * 
     * @return (type) double
     */
    public double getPosX(){
        return this.posX;
    }

    /**Permet de retourner la position Y d'un alien
     * 
     * @return (type) double
     */
    public double getPosY(){
        return this.posY;
    }

    /**Permet de retourner un boolean permettant de savoir si un alien est mort ou non
     * 
     * @return (type) boolean
     */
    public boolean getMort(){
        return this.estMort;
    }

    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines a = new EnsembleChaines();
        a.ajouteChaine((int)posX, (int)posY - 1, "   ▀▄   ▄▀   ");
        a.ajouteChaine((int)posX, (int)posY - 2, "  ▄█▀███▀█▄  ");
        a.ajouteChaine((int)posX, (int)posY - 3, " █▀███████▀█ ");
        a.ajouteChaine((int)posX, (int)posY - 4, " █ █▀▀▀▀▀█ █ ");
        a.ajouteChaine((int)posX, (int)posY - 5, "    ▀▀ ▀▀    ");
        return a;
    }

    public EnsembleChaines getEnsembleChaines2(){
        EnsembleChaines a = new EnsembleChaines();
        a.ajouteChaine((int)posX, (int)posY - 1, "  ▄ ▀▄     ▄▀ ▄  ");
        a.ajouteChaine((int)posX, (int)posY - 2, "   █▄███████▄█   ");
        a.ajouteChaine((int)posX, (int)posY - 3, "   ███▄███▄███   ");
        a.ajouteChaine((int)posX, (int)posY - 4, "   ▀█████████▀   ");
        a.ajouteChaine((int)posX, (int)posY - 5, "    ▄▀     ▀▄    ");
        return a;
    }


    /**Permet de faire evoluer les aliens en fonction du nombre de tours
     * 
     */
    public void evolue(){
        System.out.println(this.nbTour);
        if (this.nbTour < 100){
            this.posX += 0.1;
        }
        if (this.nbTour == 100){
            this.posY -= 1;
        }
        if(this.nbTour > 100){
            this.posX -= 0.1;
        }
        if (this.nbTour == 200){
            this.nbTour = 0;
        }
        this.nbTour+=1;
    }

    
    /**Vérifie si les coordonnées (a,b) sont situées sur l'image de l'alien.
    *
    *@param a un double représentant la coordonnée horizontale à vérifier.
    *@param b un double représentant la coordonnée verticale à vérifier.
    *@return (type) boolean
    */
    public boolean contient(double a, double b) {
        double alienX = this.getPosX();
        double alienY = this.getPosY();
        double largeur = 13; // Largeur de l'alien
        double hauteur = 5; // Hauteur de l'alien
    
        // Vérifie si les coordonnées (x,y) sont sur l'image de l'alien
        if (a >= alienX && a < alienX + largeur
                && b >= alienY && b < alienY + hauteur) {
            return true;
        }
        return false;
    }


}


















