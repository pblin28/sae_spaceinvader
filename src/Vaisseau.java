public class Vaisseau {
    private double posX;

    /**Permet de déplacer le vaisseau en vérifiant les limites de la fenêtre de jeu
     * 
     * @param dx correspondant à la distance à laquelle le vaisseau doit être déplacé
     */
    public void deplace(double dx) {
        double nouvellePosition = posX + dx;
        double limiteGauche = 0;
        double limiteDroite = 100 - 13; // 13 est la largeur du vaisseau
        if (nouvellePosition >= limiteGauche && nouvellePosition <= limiteDroite) {
            posX = nouvellePosition;
        }
    }

    /**Permet de retourner la chaine du vaisseau
     * 
     * @return (type) EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines() {
        EnsembleChaines e = new EnsembleChaines();
        e.ajouteChaine((int)posX, (int)5.0, "      ▄      ");
        e.ajouteChaine((int)posX, (int)4.0, "     ███     ");
        e.ajouteChaine((int)posX, (int)3.0, "▄███████████▄");
        e.ajouteChaine((int)posX, (int)2.0, "█████████████");
        e.ajouteChaine((int)posX, (int)1.0, "█████████████");

        return e;
    }

    /**Permet de retourner la position du canon du vaisseau
     * 
     * @return (type) double
     */
    public double positionCanon() {
        return this.posX + 6;
    }
}
