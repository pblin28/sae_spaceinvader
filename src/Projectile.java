public class Projectile {
    private double positionX;
    private double positionY;

    /**Constructeur de la classe Projectile
     * 
     * @param x position x initiale d'un projectile
     */
    public Projectile(double x) {
        positionX = x;
        positionY = 1;
    }

    /**Permet de faire évoluer le projectile
     * 
     */
    public void evolue() {
        positionY += 0.2;
    }

    /**Permet de retourner la position Y d'un projectile
     * 
     * @return (type) double
     */
    public double getPositionY() {
        return positionY;
    }

    /**Permet de retourner la position X d'un projectile
     * 
     * @return (type) double
     */
    public double getPositionX(){
        return positionX;
    }

    /**Permet de retourner un projectile avec sa position X et Y
     * 
     * @return (type) EnsembleChaines
     */
    public EnsembleChaines getEnsembleChaines() {
        EnsembleChaines e = new EnsembleChaines();
        e.ajouteChaine((int)positionX, (int)positionY, "ᐤ");
        return e;
    }

}
