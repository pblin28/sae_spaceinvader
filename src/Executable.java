import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.application.Application;
import javafx.application.Platform;



public class Executable extends Application {
    private Pane root;
    private Group caracteres;
    private GestionJeu gestionnaire;
    private int hauteurTexte;
    private int largeurCaractere;
    public static void main(String[] args) {
        launch(args);
    }

    
    private void afficherCaracteres(){
        caracteres.getChildren().clear();
        int hauteur = (int) root.getHeight();
        if(gestionnaire.isPartieDebutee() == true){
            for( ChainePositionnee c : gestionnaire.getChaines().chaines)
        {
            Text t = new Text (c.x*largeurCaractere,hauteur - c.y*hauteurTexte, c.c);
            t.setFont(Font.font ("Monospaced", 10));
            caracteres.getChildren().add(t);
            }
        }
    }    

    private void lancerAnimation() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0),
                    new EventHandler<ActionEvent>() {
                        @Override public void handle(ActionEvent actionEvent) {
                            gestionnaire.jouerUnTour();
                            afficherCaracteres();
                        }
                    }),
                new KeyFrame(Duration.seconds(0.025))
                );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }


    @Override
        public void start(Stage primaryStage) {
            primaryStage.setTitle("IUTO Space Invader");
            Image imageFond = new Image("file:img/imagedefond.jpg");
            BackgroundSize backgroundSize = new BackgroundSize(660, 660, false, false, false, false);
            BackgroundImage backgroundImage = new BackgroundImage(imageFond, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, backgroundSize);
            Background background = new Background(backgroundImage);
            caracteres = new Group();
            root= new AnchorPane(caracteres);
            gestionnaire = new GestionJeu();
            Text t=new Text("█");
            t.setFont(Font.font("Monospaced",10));
            hauteurTexte =(int) t.getLayoutBounds().getHeight();
            largeurCaractere = (int) t.getLayoutBounds().getWidth();

            Text bienvenueText = new Text("BIENVENUE");
            bienvenueText.setFont(Font.font("Monospaced", 75));
            bienvenueText.setFill(Color.WHITE);
            bienvenueText.setX(gestionnaire.getLargeur() * largeurCaractere / 2 - bienvenueText.getLayoutBounds().getWidth() / 2);
            bienvenueText.setY(gestionnaire.getHauteur() * hauteurTexte / 3);

            Button jouerButton = new Button("Jouer");
            Button quitterButton = new Button("Quitter");


            VBox buttonBox = new VBox(70, jouerButton, quitterButton);
            buttonBox.setAlignment(Pos.CENTER);
            buttonBox.setLayoutX(gestionnaire.getLargeur() * largeurCaractere / 2.3 - buttonBox.getWidth() / 2.3);
            buttonBox.setLayoutY(gestionnaire.getHauteur() * hauteurTexte / 2.3);

            Group groupeElementsDebutPartie = new Group(bienvenueText, buttonBox);

            jouerButton.setOnAction(ActionEvent ->{
                // Lancement de la partie
                if(gestionnaire.isPartieDebutee() == false){
                    gestionnaire.setPartieCommencee();
                    afficherCaracteres();
                    root.getChildren().remove(groupeElementsDebutPartie);
            }});

            quitterButton.setOnAction(ActionEvent -> {
                // Fermeture de la fenêtre
                primaryStage.close(); 
            });


            // Ajout des éléments à la racine
            root.getChildren().addAll(groupeElementsDebutPartie);



            Scene scene = new Scene(root,gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
            scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
                if(key.getCode()==KeyCode.LEFT)
                    gestionnaire.toucheGauche();
                if(key.getCode()==KeyCode.RIGHT)
                    gestionnaire.toucheDroite();
                if(key.getCode()==KeyCode.SPACE)
                    gestionnaire.toucheEspace();
            });
            root.setBackground(background);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.show();
            lancerAnimation();

        }
       
}
