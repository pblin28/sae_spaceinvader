public class Score {
    /** le score */
    private int score;
    /** Creation du score */
    public Score() {
        score = 0;
    }
    /**
     *  retourne le score
     * @return int
     */
    public int getScore() {
        return score;
    }
    /**
     * ajoute une valeur au score
     * @param valeur int
     */
    public void ajouter(int valeur) {
        score += valeur;
    }
    
    public String toString(){
        return "Votre Score est de " +this.getScore() +"!";
    }
}
