import java.util.List;
import javax.swing.*;
import javax.imageio.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.scene.media.*;
import javafx.scene.media.MediaPlayer;

public class GestionJeu {
    private int largeur;
    private int hauteur;
    private double positionX;
    private double positionY;
    private EnsembleChaines chaines;
    private Vaisseau vaisseau;
    private List<Projectile> projectiles = new ArrayList<>();
    private List<Alien> aliens = new ArrayList<>();
    private long dernierTir = 0;
    private long delaiTir = 500; // temps minimum entre chaque tir en millisecondes
    private Score score;
    private long dernierTempsForme;
    private int alienSt;
    private boolean partieDebutee;

    /**
     * Constructeur de la classe GestionJeu
     */
    public GestionJeu() {
        this.largeur = 100;
        this.hauteur = 60;
        this.chaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau();
        this.score = new Score();
        creationAliens();
    }


    /**Permet la création des aliens
     * 
     */
    public void creationAliens(){
        this.aliens = new ArrayList<>();
        double positionX = 8;
        double positionY = 50;
        int alienCount = 0;
        while (alienCount < 10) {
            this.aliens.add(new Alien(positionX, positionY));
            alienCount++;
            positionX += 17;
            if (positionX > 85) {
                positionX = 8;
                positionY -= 10;
            }
        }
    }

    /**Permet de mettre partieDebutee à true
     * 
     * @return (type) boolean
     */
    public boolean setPartieCommencee(){
        return this.partieDebutee = true;
    }


    /**Permet de retourner un boolean qui nous fait savoir si la partie a débutée ou non
     * 
     * @return (boolean)
     */
    public boolean isPartieDebutee(){
        return this.partieDebutee;
    }


    /**Permet de retourner la hauteur de la fenêtre de jeu
     * 
     * @return (type) int
     */
    public int getHauteur() {
        return this.hauteur;
    }

    /**Permet de retourner la largeur de la fenêtre de jeu
     * 
     * @return (type) int
     */
    public int getLargeur() {
        return this.largeur;
    }

    /**Permet de recuperer toutes les chaines nécessaires afin qu'elles apparaissent durant la partie
     * 
     * @return (type) EnsembleChaines
     */
    public EnsembleChaines getChaines() {
        EnsembleChaines e = new EnsembleChaines();
        e.union(chaines);
        e.union(vaisseau.getEnsembleChaines());
        for (Projectile p : projectiles) {
            e.union(p.getEnsembleChaines());
        }
        for (Alien a : aliens){
            e.union(changerAliens());
            }
            return e;
        }
        
    

    /**Permet de déplacer le vaisseau vers la gauche avec la flèche de gauche
     * 
     */
    public void toucheGauche() {
        vaisseau.deplace(-1);
    }

    /**Permet de déplacer le vaisseau vers la droite avec la flèche de droite
     * 
     */
    public void toucheDroite() {
        vaisseau.deplace(1);
    }

    /**Permet de tirer un projectile avec la barre espace
     * 
     */
    public void toucheEspace() {
        System.out.println("Appui sur la touche espace");
        long tirActuel = System.currentTimeMillis();
        if (tirActuel - dernierTir >= delaiTir) {
            projectiles.add(new Projectile(vaisseau.positionCanon()));
            dernierTir = tirActuel;
            final File file = new File("sons/son_canon_2.mp3"); 
            final Media media = new Media(file.toURI().toString()); 
            final MediaPlayer mediaPlayer = new MediaPlayer(media); 
            mediaPlayer.play();
        }
    }

    /**Permet de jouer un tour et toutes les actions qui sont réalisé pendant un tour de jeu
     * 
     */
    public void jouerUnTour() {
            for (Projectile p : projectiles) {
                p.evolue();
                if (p.getPositionY() >= getHauteur()) {
                    projectiles.remove(p);
                }
            }
            
        for (Alien a : this.aliens){
            a.evolue();
        }    
        score.ajouter(1);
        for (int i = 0; i < projectiles.size(); i++) {
            Projectile p = projectiles.get(i);
            // Vérification pour chaque alien
            for (int j = 0; j < aliens.size(); j++) {
                Alien a = aliens.get(j);
                if (a.contient(p.getPositionX(), p.getPositionY())) {
                    aliens.remove(j);
                    final File file = new File("sons/arcade_explosion.mp3"); 
                    final Media media = new Media(file.toURI().toString()); 
                    final MediaPlayer mediaPlayer = new MediaPlayer(media); 
                    mediaPlayer.play();
                    projectiles.remove(i);
                    System.out.println("TOUCHE");
                    }
                }
            }
        }

        public EnsembleChaines changerAliens(){
            EnsembleChaines e = new EnsembleChaines();
            long tempsActuel = System.currentTimeMillis();
            if(tempsActuel - dernierTempsForme >= 2000){
                this.alienSt = (this.alienSt == 1 ) ? 2:1;
                this.dernierTempsForme = tempsActuel;
            }
            for (Alien a : this.aliens){
                if (this.alienSt == 1){

                e.union(a.getEnsembleChaines());
            } else {
                e.union(a.getEnsembleChaines2());
            }
        }
        return e;        
        
    }

}













